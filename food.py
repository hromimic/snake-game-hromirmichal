import pygame
import random
import time
from settings import GRID_SIZE, GRID_WIDTH, GRID_HEIGHT, RED
from typing import List, Tuple

class Food:
    """
    Třída reprezentující jídlo ve hře Snake.

    Atributy:
        position (Tuple[int, int]): Pozice jídla na herní mřížce, kde každá pozice je reprezentována jako tuple (x, y).

    Metody:
        random_position() -> Tuple[int, int]: Vygeneruje náhodnou pozici pro jídlo.
        draw(surface: pygame.Surface) -> None: Nakreslí jídlo na zadaný povrch.
    """

    def __init__(self):
        """
        Inicializuje novou instanci jídla a nastaví její pozici na náhodnou.
        """

        self.position = self.random_position()
    
    def random_position(self) -> Tuple[int, int]:
        """
        Vygeneruje náhodnou pozici pro jídlo.

        Návratové hodnoty:
            Tuple[int, int]: Náhodná pozice jídla na herní mřížce.
        """

        return (random.randint(0, GRID_WIDTH - 1), random.randint(0, GRID_HEIGHT - 1))
    
    def draw(self, surface: pygame.Surface) -> None:
        """
        Nakreslí jídlo na zadaný povrch.

        Parametry:
            surface (pygame.Surface): Povrch, na který bude jídlo nakresleno.
        """

        rect = pygame.Rect(self.position[0] * GRID_SIZE, self.position[1] * GRID_SIZE, GRID_SIZE, GRID_SIZE)
        pygame.draw.rect(surface, RED, rect)

class FoodFactory:
    """
    Třída pro správu tvorby a správy jídla ve hře Snake.

    Atributy:
        food_list (List[Food]): Seznam všech aktuálně existujících kusů jídla.
        max_food (int): Maximální počet kusů jídla, které mohou existovat současně.
        base_interval (float): Základní interval mezi spawny jídla v sekundách.
        k (float): Konstantní násobitel pro exponenciální růst.
        power (float): Exponent pro exponenciální růst.
        last_spawn_time (float): Čas posledního spawnu jídla.

    Metody:
        create_food() -> None: Vytvoří nový kus jídla a přidá ho do seznamu.
        update() -> None: Aktualizuje čas spawnu a případně vytvoří nový kus jídla.
        draw_food(surface: pygame.Surface) -> None: Nakreslí všechny kusy jídla na zadaný povrch.
        remove_food(food: Food) -> None: Odstraní zadaný kus jídla ze seznamu.
    """

    def __init__(self, max_food: int = 5):
        """
        Inicializuje novou instanci továrny na jídlo.

        Parametry:
            max_food (int): Maximální počet kusů jídla, které mohou existovat současně (defaultně 5).
        """

        self.food_list = []
        self.max_food = max_food
        self.base_interval = 2  # Základní interval v sekundách
        self.k = 3  # Konstantní násobitel pro exponenciální růst
        self.power = 1.2  # Exponent pro exponenciální růst
        self.last_spawn_time = time.time()

    def create_food(self) -> None:
        """
        Vytvoří nový kus jídla a přidá ho do seznamu, pokud aktuální počet kusů jídla je menší než maximální počet.
        """

        if len(self.food_list) < self.max_food:
            new_food = Food()
            self.food_list.append(new_food)

    def update(self) -> None:
        """
        Aktualizuje čas spawnu a případně vytvoří nový kus jídla podle exponenciálního růstu.

        Poznámky:
            - Interval mezi spawny jídla je dynamicky upravován na základě počtu existujících kusů jídla.
        """

        current_time = time.time()
        elapsed_time = current_time - self.last_spawn_time
        
        food_count = len(self.food_list)
        
        if food_count > 0:
            # Výpočet exponenciálního zvýšení
            exponential_increase = self.k * (food_count ** self.power)
            spawn_interval = self.base_interval + exponential_increase
        else:
            spawn_interval = self.base_interval

        if elapsed_time >= spawn_interval:
            self.create_food()
            self.last_spawn_time = current_time

    def draw_food(self, surface: pygame.Surface) -> None:
        """
        Nakreslí všechny kusy jídla na zadaný povrch.

        Parametry:
            surface (pygame.Surface): Povrch, na který budou všechny kusy jídla nakresleny.
        """

        for food in self.food_list:
            food.draw(surface)

    def remove_food(self, food: Food) -> None:
        """
        Odstraní zadaný kus jídla ze seznamu.

        Parametry:
            food (Food): Kus jídla, který má být odstraněn.
        """

        if food in self.food_list:
            self.food_list.remove(food)

