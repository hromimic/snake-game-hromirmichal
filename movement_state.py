from abc import ABC, abstractmethod

class MovementState(ABC):
    """
    Abstraktní třída reprezentující stav pohybu hada.

    Metody:
        move(snake): Abstraktní metoda, která určuje, jak se had pohybuje v daném stavu.
    """

    @abstractmethod
    def move(self, snake: 'Snake') -> None:
        """
        Určuje, jak se had pohybuje v daném stavu.

        Args:
            snake (Snake): Objekt hada, který má být posunut.
        """
        
        pass

class MovingUpState(MovementState):
    def move(self, snake: 'Snake') -> None:
        """
        Posune hada směrem nahoru.

        Args:
            snake (Snake): Objekt hada, který má být posunut.
        """

        head_x, head_y = snake.positions[0]
        new_position = (head_x, head_y - 1)
        if not snake.grow:
            snake.positions.pop()
        else:
            snake.grow = False
        snake.positions.insert(0, new_position)

class MovingDownState(MovementState):
    def move(self, snake: 'Snake') -> None:
        """
        Posune hada směrem dolů.

        Args:
            snake (Snake): Objekt hada, který má být posunut.
        """

        head_x, head_y = snake.positions[0]
        new_position = (head_x, head_y + 1)
        if not snake.grow:
            snake.positions.pop()
        else:
            snake.grow = False
        snake.positions.insert(0, new_position)

class MovingLeftState(MovementState):
    def move(self, snake: 'Snake') -> None:
        """
        Posune hada směrem vlevo.

        Args:
            snake (Snake): Objekt hada, který má být posunut.
        """

        head_x, head_y = snake.positions[0]
        new_position = (head_x - 1, head_y)
        if not snake.grow:
            snake.positions.pop()
        else:
            snake.grow = False
        snake.positions.insert(0, new_position)

class MovingRightState(MovementState):
    def move(self, snake: 'Snake') -> None:
        """
        Posune hada směrem vpravo.

        Args:
            snake (Snake): Objekt hada, který má být posunut.
        """

        head_x, head_y = snake.positions[0]
        new_position = (head_x + 1, head_y)
        if not snake.grow:
            snake.positions.pop()
        else:
            snake.grow = False
        snake.positions.insert(0, new_position)

