import pygame
from typing import Tuple  # Přidání potřebného importu pro Tuple
from settings import GRID_SIZE  # Import GRID_SIZE for block size

"""
Modul obsahující pomocné funkce pro vykreslování ve hře Snake.

Tento modul obsahuje funkce pro vykreslování hada a textu na herní plochu pomocí knihovny Pygame.

Funkce:
    draw_snake(surface: pygame.Surface, snake: Snake) -> None:
        Vykreslí hada na zadaném povrchu.
        
        Parametry:
            surface (pygame.Surface): Povrch, na který bude had vykreslen.
            snake (Snake): Objekt hada, který obsahuje pozice a barvu hada.

        Vedlejší účinky:
            - Na zadaném povrchu se vykreslí čtverce reprezentující pozice hada.

    draw_text(surface: pygame.Surface, text: str, size: int, color: Tuple[int, int, int], position: Tuple[int, int]) -> None:
        Vykreslí text na zadaném povrchu.

        Parametry:
            surface (pygame.Surface): Povrch, na který bude text vykreslen.
            text (str): Text, který se má vykreslit.
            size (int): Velikost písma.
            color (Tuple[int, int, int]): Barva textu ve formátu RGB.
            position (Tuple[int, int]): Pozice středu textu na povrchu.

        Vedlejší účinky:
            - Na zadaném povrchu se vykreslí text na zadané pozici.
"""

import pygame
from settings import GRID_SIZE  # Import GRID_SIZE for block size

def draw_snake(surface: pygame.Surface, snake: 'Snake') -> None:
    """
    Vykreslí hada na zadaném povrchu.
    
    Parametry:
        surface (pygame.Surface): Povrch, na který bude had vykreslen.
        snake (Snake): Objekt hada, který obsahuje pozice a barvu hada.
    
    Vedlejší účinky:
        - Na zadaném povrchu se vykreslí čtverce reprezentující pozice hada.
    """
    
    for position in snake.positions:
        rect = pygame.Rect(position[0] * GRID_SIZE, position[1] * GRID_SIZE, GRID_SIZE, GRID_SIZE)
        pygame.draw.rect(surface, snake.color, rect)

def draw_text(surface: pygame.Surface, text: str, size: int, color: Tuple[int, int, int], position: Tuple[int, int]) -> None:
    """
    Vykreslí text na zadaném povrchu.
    
    Parametry:
        surface (pygame.Surface): Povrch, na který bude text vykreslen.
        text (str): Text, který se má vykreslit.
        size (int): Velikost písma.
        color (Tuple[int, int, int]): Barva textu ve formátu RGB.
        position (Tuple[int, int]): Pozice středu textu na povrchu.
    
    Vedlejší účinky:
        - Na zadaném povrchu se vykreslí text na zadané pozici.
    """

    font = pygame.font.Font(None, size)
    text_surface = font.render(text, True, color)
    text_rect = text_surface.get_rect(center=position)
    surface.blit(text_surface, text_rect)

