from typing import List, Tuple
from settings import GRID_WIDTH, GRID_HEIGHT, GREEN
from movement_state import MovingUpState, MovingDownState, MovingLeftState, MovingRightState

class Snake:
    """
    Třída reprezentující hada ve hře Snake.

    Atributy:
        color (Tuple[int, int, int]): Barva hada, defaultně GREEN.
        positions (List[Tuple[int, int]]): Seznam pozic hada, kde každá pozice je reprezentována jako tuple (x, y).
        direction (MovementState): Aktuální stav pohybu hada.
        grow (bool): Flag určující, zda má had růst při pohybu.
        direction_queue (List[Tuple[int, int]]): Fronta směrů, které čekají na zpracování.
        speed (int): Rychlost pohybu hada.

    Metody:
        move(): Posune hada v aktuálním směru a zpracovává směr v frontě.
        change_direction(new_direction: Tuple[int, int]) -> None: Změní směr pohybu hada.
        queue_direction(new_direction: Tuple[int, int]) -> None: Přidá nový směr do fronty směrů.
        is_opposite_direction(new_direction: Tuple[int, int]) -> bool: Zkontroluje, zda je nový směr opačný k aktuálnímu směru.
        grow_snake() -> None: Aktivuje růst hada při dalším pohybu.
        check_collision() -> bool: Zkontroluje, zda došlo ke kolizi hada se stěnami nebo sám se sebou.
    """

    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super(Snake, cls).__new__(cls)
        return cls._instance

    def __init__(self, color: Tuple[int, int, int] = GREEN, initial_length: int = 3, speed: int = 10) -> None:
        """
        Inicializuje novou instanci hada, pokud ještě neexistuje.

        Parametry:
            color (Tuple[int, int, int]): Barva hada (defaultně GREEN).
            initial_length (int): Počáteční délka hada (defaultně 3).
            speed (int): Rychlost pohybu hada (defaultně 10).
        """
        
        if not hasattr(self, 'initialized'):  # Zkontroluje, zda je instance již inicializována
            self.color = color
            self.positions: List[Tuple[int, int]] = [(GRID_WIDTH // 2, GRID_HEIGHT // 2) for _ in range(initial_length)]
            self.direction = MovingUpState()  # Výchozí směr
            self.speed = speed
            self.grow = False
            self.direction_queue: List[Tuple[int, int]] = []  # Fronta směrů

            # Mapování směrů na stavy
            self.direction_map = {
                (0, -1): MovingUpState(),
                (0, 1): MovingDownState(),
                (-1, 0): MovingLeftState(),
                (1, 0): MovingRightState()
            }

            self.initialized = True  # Značí, že instance byla inicializována

    def reset_instance(self) -> None:
        """
        Metoda pro resetování instance hada. Používá se hlavně v testovacím prostředí.
        """

        type(self)._instance = None
        del self.initialized

    def move(self) -> None:
        """
        Posune hada v aktuálním směru a zpracovává směry v frontě.

        Pokud je ve frontě směrů (`direction_queue`), vybere první platný směr, změní aktuální směr hada na tento nový směr 
        a odstraní tento směr z fronty. Poté se had posune v aktuálním směru.

        Pokud je fronta směrů prázdná, had se jednoduše posune v aktuálním směru bez změny směru.

        Vedlejší účinky:
            - Mění pozice hada v závislosti na aktuálním směru.
            - Může změnit směr pohybu hada na základě fronty směrů.
        """

        # Pokud je ve frontě nějaký směr, vybereme první platný a aplikujeme ho
        if self.direction_queue:
            new_direction = self.direction_queue.pop(0)
            self.change_direction(new_direction)
        self.direction.move(self)

    def change_direction(self, new_direction: Tuple[int, int]) -> None:
        """
        Změní směr pohybu hada.

        Parametry:
            new_direction (Tuple[int, int]): Nový směr pohybu hada jako tuple (x, y).

        Poznámky:
            - Pokud je nový směr platný a není opačný k aktuálnímu směru, změní směr pohybu hada.
        """

        if new_direction in self.direction_map and not self.is_opposite_direction(new_direction):
            self.direction = self.direction_map[new_direction]

    def queue_direction(self, new_direction: Tuple[int, int]) -> None:
        """
        Přidá nový směr do fronty směrů.

        Parametry:
            new_direction (Tuple[int, int]): Nový směr pohybu hada jako tuple (x, y).

        Poznámky:
            - Pokud je nový směr platný a není opačný k aktuálnímu směru, přidá se do fronty.
        """

        if new_direction in self.direction_map and not self.is_opposite_direction(new_direction):
            self.direction_queue.append(new_direction)

    def is_opposite_direction(self, new_direction: Tuple[int, int]) -> bool:
        """
        Zkontroluje, zda je nový směr opačný k aktuálnímu směru.

        Parametry:
            new_direction (Tuple[int, int]): Nový směr pohybu hada jako tuple (x, y).

        Návratové hodnoty:
            bool: `True` pokud je nový směr opačný k aktuálnímu směru, jinak `False`.
        """

        current_direction = self.direction
        opposite_directions = {
            MovingUpState: (0, 1),
            MovingDownState: (0, -1),
            MovingLeftState: (1, 0),
            MovingRightState: (-1, 0)
        }
        return new_direction == opposite_directions[type(current_direction)]

    def grow_snake(self) -> None:
        """
        Aktivuje růst hada při dalším pohybu.

        Poznámky:
            - Při dalším pohybu hada se jeho délka zvýší o jeden segment.
        """

        self.grow = True
    
    def check_collision(self) -> bool:
        """
        Zkontroluje, zda došlo ke kolizi hada se stěnami nebo sám se sebou.

        Návratové hodnoty:
            bool: `True` pokud došlo ke kolizi, jinak `False`.
        """

        head_x, head_y = self.positions[0]
        if head_x < 0 or head_x >= GRID_WIDTH or head_y < 0 or head_y >= GRID_HEIGHT:
            return True
        if (head_x, head_y) in self.positions[1:]:
            return True
        return False

