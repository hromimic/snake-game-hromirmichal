import unittest
from snake import Snake
from settings import GRID_WIDTH, GRID_HEIGHT

class TestSnake(unittest.TestCase):
    
    def setUp(self) -> None:
        """
        Příprava prostředí pro testy. Vytváří nový objekt hada před každým testem.
        """
        
        Snake().reset_instance()  # Resetuje Singleton instanci
        self.snake = Snake()
    
    def test_initial_position(self) -> None:
        """
        Testuje, zda had začíná na správné pozici.

        Očekávaný výsledek:
            Had by měl začínat ve středu herního pole s výchozí délkou 3 segmenty.
        """

        center_x = GRID_WIDTH // 2
        center_y = GRID_HEIGHT // 2
        expected_positions = [(center_x, center_y) for _ in range(3)]
        self.assertEqual(self.snake.positions, expected_positions, "Initial snake positions are not correct")

    def test_move_up(self) -> None:
        """
        Testuje, zda se had správně pohybuje směrem nahoru.

        Scénář:
            1. Nastavíme počáteční pozici hada.
            2. Změníme směr na nahoru.
            3. Posuneme hada a ověříme novou pozici.

        Očekávaný výsledek:
            Had by se měl posunout o jednu jednotku nahoru.
        """

        self.snake.positions = [(GRID_WIDTH // 2, GRID_HEIGHT // 2)]
        self.snake.change_direction((0, -1))  # Nastavit směr na nahoru
        
        # Posunout hada a zkontrolovat novou pozici
        self.snake.move()
        expected_position = (GRID_WIDTH // 2, GRID_HEIGHT // 2 - 1)
        self.assertEqual(self.snake.positions[0], expected_position, "Snake did not move up correctly")

    def test_move_left(self) -> None:
        """
        Testuje, zda se had správně pohybuje směrem vlevo.

        Scénář:
            1. Nastavíme počáteční pozici hada.
            2. Změníme směr na vlevo.
            3. Posuneme hada a ověříme novou pozici.

        Očekávaný výsledek:
            Had by se měl posunout o jednu jednotku vlevo.
        """

        self.snake.positions = [(GRID_WIDTH // 2, GRID_HEIGHT // 2)]
        self.snake.change_direction((-1, 0))  # Nastavit směr na vlevo
        
        # Posunout hada a zkontrolovat novou pozici
        self.snake.move()
        expected_position = (GRID_WIDTH // 2 - 1, GRID_HEIGHT // 2)
        self.assertEqual(self.snake.positions[0], expected_position, "Snake did not move left correctly")

    def test_move_right(self) -> None:
        """
        Testuje, zda se had správně pohybuje směrem vpravo.

        Scénář:
            1. Nastavíme počáteční pozici hada.
            2. Změníme směr na vpravo.
            3. Posuneme hada a ověříme novou pozici.

        Očekávaný výsledek:
            Had by se měl posunout o jednu jednotku vpravo.
        """

        self.snake.positions = [(GRID_WIDTH // 2, GRID_HEIGHT // 2)]
        self.snake.change_direction((1, 0))  # Nastavit směr na vpravo
        
        # Posunout hada a zkontrolovat novou pozici
        self.snake.move()
        expected_position = (GRID_WIDTH // 2 + 1, GRID_HEIGHT // 2)
        self.assertEqual(self.snake.positions[0], expected_position, "Snake did not move right correctly")

    def test_grow_snake(self) -> None:
        """
        Testuje, zda se had správně zvětšuje po sežrání jídla.

        Scénář:
            1. Zvětšíme hada.
            2. Posuneme hada a ověříme, zda se zvětšil.

        Očekávaný výsledek:
            Had by měl mít jeden segment více než před zvětšením.
        """

        self.snake.positions = [(GRID_WIDTH // 2, GRID_HEIGHT // 2)]
        self.snake.grow_snake()  # Spustit růst
        
        # Posunout hada a zkontrolovat délku
        self.snake.move()
        self.assertEqual(len(self.snake.positions), 2, "Snake did not grow correctly after moving")

if __name__ == "__main__":
    unittest.main()

