# python -m unittest discover -s tests

"""
Tento balíček obsahuje testy pro projekt.

Testy jsou organizovány do následujících souborů:
- test_snake.py: Testy pro třídu Snake.
- test_food.py: Testy pro třídu Food a FoodFactory.
- test_collision.py: Testy pro detekci kolizí.
- test_integration.py: Integrační testy pro ověření funkce mezi různými komponentami.

Použijte příkaz `python -m unittest discover -s tests`, abyste spustili všechny testy v tomto balíčku.
"""

