"""
Nastavení pro hru Snake.

Tento modul obsahuje konfigurační nastavení pro hru, včetně velikosti okna, velikosti mřížky, barev a rychlosti hry.

Konstanty:
    WINDOW_WIDTH (int): Šířka herního okna v pixelech.
    WINDOW_HEIGHT (int): Výška herního okna v pixelech.
    GRID_SIZE (int): Velikost jednotlivých buněk mřížky v pixelech.
    GRID_WIDTH (int): Počet buněk mřížky v šířce okna.
    GRID_HEIGHT (int): Počet buněk mřížky v výšce okna.
    BLACK (Tuple[int, int, int]): Barva černá (RGB).
    WHITE (Tuple[int, int, int]): Barva bílá (RGB).
    GREEN (Tuple[int, int, int]): Barva zelená (RGB).
    RED (Tuple[int, int, int]): Barva červená (RGB).
    FPS (int): Rychlost hry v snímcích za sekundu.
"""

# Nastavení velikosti okna
WINDOW_WIDTH, WINDOW_HEIGHT = 700, 700
GRID_SIZE = 25
GRID_WIDTH = WINDOW_WIDTH // GRID_SIZE
GRID_HEIGHT = WINDOW_HEIGHT // GRID_SIZE

# Barvy
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

# Nastavení FPS (rychlosti hry)
FPS = 10

